/*
 * Start mongod.
 * Run using 'node index.js'
 * Or, nodemon index.js. Nodemon watches for changes and automatically restarts server.
 * Nodemon is OS specific so we can't push the package file into repostitory.
 */


/*
 *  Using http
 */
/*
// Runs for every request to Node
var http = require('http'); // import with ES6

http.createServer(function(req, res) {
    res.writeHead(200, {
        'Content-Type' : 'text/plain'
    })
    
    res.end('Hello World\n');
}).listen(3000, '127.0.0.1');
console.log('Server running at http://127.0.0.1:3000/');
*/

/*
 *  Using Express
 */
// Express is sugar coating on top of http.
var express = require('express');
var app = express();

var bodyParser = require('body-parser');


var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/cats');

// Tell it to read json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

var catRoutes = require('./routes/cat.js')(app);    // Require cats_routes.js in the same folder

// Express says for get, send back this. Before, http.createServer was for any request.
app.get('/', function(req, res) {
    // res.send('Hello World!');   // Defaults to Content-Type:text/html;
    res.json( {hello: 'cat world'} );
});

var server = app.listen(3000, function() {
   console.log('Server running at http://127.0.0.1:3000/'); 
});
