// Requests allows http calls to secondary servers.
var r = require('request').defaults({
    json: true  // Send data out as json
});

var async = require('async');
var redis = require('redis');

var client = redis.createClient(6379, '127.0.0.1'); // Redis will need to be installed and started on Windows. See readme.

module.exports = function(app) {
    
    // Pets endpoint
    /* Read */
    app.get('/pets', function(req, res) {
        async.parallel({
            // Wait for all to return, then run the 'awaiting' function below.
            cat: function(callback) {
                r({uri: 'http://localhost:3000/cat'}, function(error, response, body) { // Request call to cat
                    if(error) {
                        callback({service: 'cat', error: error});
                        return;
                    }            
                    if(!error && response.statusCode == 200) {
                        callback(null, body.data);  // null error.
                    } else {
                        callback(response.statusCode);
                    }           
                });
            },
            dog: function(callback) {
                // Talk to Radis first and see if it has the key value pair. Otherwise hit the database.
                // Set end point as key in cache.
                client.get('http://localhost:3001/dog', function(error, dog) {
                    if(error) { throw error; };
                    
                    if(dog) {
                        callback(null, JSON.parse(dog));
                    } else {
                        r({uri: 'http://localhost:3001/dog'}, function(error, response, body) { // Request call to dog
                            if(error) {
                                // returned with error
                                callback({service: 'dog', error: error});
                                return;
                            }    
                                                
                            if(!error && response.statusCode === 200) {
                                callback(null, body.data);
                                ///client.set('http://localhost:3001/dog', JSON.stringify(body.data), function(error) {
                                client.setex(req.params.id, 10, JSON.stringify(body.data), function(error) {
                                    if(error) {throw error;};
                                })
                            } else {
                                callback(response.statusCode);
                            }
                        });
                    }
                });
                
                // r({uri: 'http://localhost:3001/dog'}, function(error, response, body) { // Request call to dog
                //     if(error) {
                //         // returned with error
                //         callback({service: 'dog', error: error});
                //         return;                        
                //     }            
                //     if(!error && response.statusCode == 200) {
                //         callback(null, body.data);
                //         //res.json(body);
                //     } else {
                //         // got a response but not what we want.
                //         callback(response.statusCode);
                //         //res.send(response.statusCode);
                //     }           
                // });
            },
        },
        
        // awaiting function
        function(error, results) {
            res.json({
               error: error,
               result: results  // for dogs and cats, joined by node 'async' package. 
            });
        });
    });
    
    
    // Ping endpoint.
    app.get('/ping', function(req, res) {
        res.json({pong: Date.now()}); 
    });
};